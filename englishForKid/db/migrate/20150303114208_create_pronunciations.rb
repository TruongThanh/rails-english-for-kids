class CreatePronunciations < ActiveRecord::Migration
  def change
    create_table :pronunciations do |t|
      t.string :quiz_id
      t.string :entertaiment_id
      t.string :title
      t.string :exercise_id
      t.string :detail_id

      t.timestamps null: false
    end
  end
end
